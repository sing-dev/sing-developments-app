import tkinter as tk
from tkinter import ttk, filedialog, messagebox
import json
import os
import subprocess
import time

# Get the current user's home directory
home_directory = os.path.expanduser("~")

# Specify the path for the hamstartdata.json file
APP_DATA_FILE = os.path.join(home_directory, "hamstartdata.json")
programs_tree = None  # Define programs_tree globally

def load_app_data():
    try:
        with open(APP_DATA_FILE, "r") as file:
            return json.load(file)
    except FileNotFoundError:
        return []

def save_app_data(data):
    with open(APP_DATA_FILE, "w") as file:
        json.dump(data, file, indent=2)

def update_app_paths():
    app_data = load_app_data()
    for app in app_data:
        if not os.path.exists(app["path"]):
            app["status"] = "Path Missing"
    save_app_data(app_data)

def validate_all_paths():
    app_data = load_app_data()
    for app in app_data:
        if not os.path.exists(app["path"]):
            app["status"] = "Path Missing"
    save_app_data(app_data)

def add_program():
    add_window = tk.Toplevel(main_window)
    add_window.title("Add Program")

    name_label = tk.Label(add_window, text="Program Name:")
    name_label.grid(row=0, column=0, pady=5, padx=5, sticky="e")

    name_entry = tk.Entry(add_window)
    name_entry.grid(row=0, column=1, pady=5, padx=5, sticky="w")

    path_label = tk.Label(add_window, text="Program Path:")
    path_label.grid(row=1, column=0, pady=5, padx=5, sticky="e")

    path_entry = tk.Entry(add_window)
    path_entry.grid(row=1, column=1, pady=5, padx=5, sticky="w")

    def browse_program_path():
        path = filedialog.askopenfilename(filetypes=[("Executable files", "*.exe")])
        path_entry.delete(0, tk.END)
        path_entry.insert(0, path)

    browse_button = tk.Button(add_window, text="Browse", command=browse_program_path, bg="lightgray")
    browse_button.grid(row=1, column=2, pady=5, padx=5)

    def add_to_table():
        name = name_entry.get()
        path = path_entry.get()

        if name and path:
            app_data.append({"name": name, "path": path, "status": "OK"})
            save_app_data(app_data)
            update_app_paths()
            refresh_table()
            add_window.destroy()

    add_button = tk.Button(add_window, text="Add Program", command=add_to_table, bg="lightblue")
    add_button.grid(row=2, column=1, pady=10)

def run_selected_program():
    selected_item = programs_tree.selection()
    if selected_item:
        selected_item = programs_tree.item(selected_item)
        selected_path = selected_item["values"][1]

        validate_all_paths()  # Validate all paths before running

        if os.path.exists(selected_path):
            subprocess.Popen(selected_path)
            status_label.config(text="Program ran successfully!", fg="green")
        else:
            status_label.config(text="Selected program path is missing!", fg="red")

def edit_program():
    selected_item = programs_tree.selection()
    if selected_item:
        selected_item = programs_tree.item(selected_item)
        selected_name = selected_item["values"][0]
        selected_path = selected_item["values"][1]

        edit_window = tk.Toplevel(main_window)
        edit_window.title("Edit Program")

        name_label = tk.Label(edit_window, text="Program Name:")
        name_label.grid(row=0, column=0, pady=5, padx=5, sticky="e")

        name_entry = tk.Entry(edit_window)
        name_entry.insert(0, selected_name)
        name_entry.grid(row=0, column=1, pady=5, padx=5, sticky="w")

        path_label = tk.Label(edit_window, text="Program Path:")
        path_label.grid(row=1, column=0, pady=5, padx=5, sticky="e")

        path_entry = tk.Entry(edit_window)
        path_entry.insert(0, selected_path)
        path_entry.grid(row=1, column=1, pady=5, padx=5, sticky="w")

        def browse_program_path():
            path = filedialog.askopenfilename(filetypes=[("Executable files", "*.exe")])
            path_entry.delete(0, tk.END)
            path_entry.insert(0, path)

        browse_button = tk.Button(edit_window, text="Browse", command=browse_program_path, bg="lightgray")
        browse_button.grid(row=1, column=2, pady=5, padx=5)

        def update_program():
            updated_name = name_entry.get()
            updated_path = path_entry.get()

            if updated_name and updated_path:
                for app in app_data:
                    if app["name"] == selected_name and app["path"] == selected_path:
                        app["name"] = updated_name
                        app["path"] = updated_path
                        break

                save_app_data(app_data)
                update_app_paths()
                refresh_table()
                edit_window.destroy()

        update_button = tk.Button(edit_window, text="Update Program", command=update_program, bg="lightblue")
        update_button.grid(row=2, column=1, pady=10)

def delete_program():
    selected_item = programs_tree.selection()
    if selected_item:
        selected_item = programs_tree.item(selected_item)
        selected_name = selected_item["values"][0]
        selected_path = selected_item["values"][1]

        confirmation = messagebox.askyesno("Confirmation", f"Do you want to delete the program '{selected_name}'?")
        if confirmation:
            for app in app_data:
                if app["name"] == selected_name and app["path"] == selected_path:
                    app_data.remove(app)
                    break

            save_app_data(app_data)
            update_app_paths()
            refresh_table()

def refresh_table():
    programs_tree.delete(*programs_tree.get_children())
    for app in app_data:
        programs_tree.insert("", tk.END, values=(app["name"], app["path"], app["status"]))

def loading_screen():
    loading_window = tk.Toplevel(main_window)
    loading_window.title("Loading")
    loading_window.attributes('-topmost', True)  # Set loading window on top

    progress_var = tk.IntVar()
    loading_bar = ttk.Progressbar(loading_window, variable=progress_var, maximum=100, length=200)
    loading_bar.grid(row=0, column=0, columnspan=3, pady=50)

    testing_label = tk.Label(loading_window, text="Testing...")
    testing_label.grid(row=1, column=0, columnspan=3, pady=10)

    validate_all_paths()  # Validate all paths during loading

    for i in range(101):
        progress_var.set(i)
        loading_bar.update()
        loading_window.update_idletasks()
        time.sleep(0.03)

    loading_window.destroy()
    main_window.deiconify()
    refresh_table()

# Load existing app data or create an empty list
app_data = load_app_data()

# Create the main window
main_window = tk.Tk()
main_window.title("HamStart")

# Create and place the treeview for displaying programs
programs_tree = ttk.Treeview(main_window, columns=("Name", "Path", "Status"), show="headings")
programs_tree.heading("Name", text="Name")
programs_tree.heading("Path", text="Path")
programs_tree.heading("Status", text="Status")
programs_tree.pack(pady=10)

# Create and place the "Add Program" button
add_program_button = tk.Button(main_window, text="Add Program", command=add_program, bg="lightyellow")
add_program_button.pack(pady=10)

# Create and place the "Run Program" button
run_program_button = tk.Button(main_window, text="Run Program", command=run_selected_program, bg="lightgreen")
run_program_button.pack(pady=10)

# Create and place the label for displaying status messages
status_label = tk.Label(main_window, text="", fg="red")
status_label.pack(pady=10)

# Create and place the "Edit Program" button
edit_program_button = tk.Button(main_window, text="Edit Program", command=edit_program, bg="lightyellow")
edit_program_button.pack(pady=5)

# Create and place the "Delete Program" button
delete_program_button = tk.Button(main_window, text="Delete Program", command=delete_program, bg="lightcoral")
delete_program_button.pack(pady=5)

# Automatically run the loading_screen function
loading_screen()

# Run the main window
main_window.mainloop()
